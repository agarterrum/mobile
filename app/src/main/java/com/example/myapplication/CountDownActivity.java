package com.example.myapplication;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
public class CountDownActivity extends AppCompatActivity {
    private static final String TAG = "lifecycle";
    private TextView countdownText;
    private CountDownTimer countdownTimer;
    //private long timeLeftInMilliseconds = 600000;
    private long timeLeftInMilliseconds;
    private long currentTimeMillis;
    private String string_date = "22-02-2019 17:00:00";
    private Date date = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "CountDownActivity создано.");
        setContentView(R.layout.activity_count_down);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    public void startTimer() {
        countdownTimer = new CountDownTimer(timeLeftInMilliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMilliseconds = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {
                countdownText.setText("Время вышло!");
            }
        }.start();
    }

    public void updateTimer() {
        long days = TimeUnit.MILLISECONDS.toDays(timeLeftInMilliseconds);
        long hours = TimeUnit.MILLISECONDS.toHours(timeLeftInMilliseconds) -
                TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(timeLeftInMilliseconds));
        long minutes =  TimeUnit.MILLISECONDS.toMinutes(timeLeftInMilliseconds) -
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeLeftInMilliseconds));
        long seconds =  TimeUnit.MILLISECONDS.toSeconds(timeLeftInMilliseconds) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeLeftInMilliseconds));

        String timeLeftText;
        timeLeftText = "До защиты проекта " + days;
        timeLeftText += " дней ";
        if (hours < 10) timeLeftText += "0";
        timeLeftText += hours;
        timeLeftText += ":";
        if (minutes < 10) timeLeftText += "0";
        timeLeftText += minutes;
        timeLeftText += ":";
        if (seconds < 10) timeLeftText += "0";
        timeLeftText += seconds;
        countdownText.setText(timeLeftText);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "CountDownActivity стал видимым.");
        currentTimeMillis = System.currentTimeMillis();
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        try {
            date = f.parse(string_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long necessaryTimeMillis = date.getTime();
        timeLeftInMilliseconds = necessaryTimeMillis - currentTimeMillis;
        countdownText = findViewById(R.id.countdown_text);
        startTimer();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "CountDownActivity получает фокус (состояние Resumed)");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "CountDownActivity приостановлено (состояние Paused)");
        countdownTimer.cancel();
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "CountDownActivity остановлено (состояние Stopped)");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "CountDownActivity перезапущено (состояние Restarted)");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "CountDownActivity уничтожено (состояние Destroyed)");
    }
}

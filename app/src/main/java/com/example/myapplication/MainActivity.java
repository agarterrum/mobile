package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    Button btnActTwo;
    Button rest;
    Button btnMqtt;
    private static final String TAG = "lifecycle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "MainActivity создано.");
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnActTwo = findViewById(R.id.btnActTwo);
        btnActTwo.setOnClickListener(this);

        btnMqtt = findViewById(R.id.btnMqtt);
        btnMqtt.setOnClickListener(this);

        rest = findViewById(R.id.restApi);
        rest.setOnClickListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnActTwo:
                Intent intent = new Intent(this, CountDownActivity.class);
                startActivity(intent);
                // TODO Call second activity
                break;
            case R.id.restApi:
                Intent intent2 = new Intent(this, RestApiActivity.class);
                startActivity(intent2);
                break;
            case R.id.btnMqtt:
                Intent intent3 = new Intent(this, MqttActivity.class);
                startActivity(intent3);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity стал видимым.");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MainActivity получает фокус (состояние Resumed)");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MainActivity приостановлено (состояние Paused)");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "MainActivity остановлено (состояние Stopped)");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "MainActivity перезапущено (состояние Restarted)");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity уничтожено (состояние Destroyed)");
    }
}

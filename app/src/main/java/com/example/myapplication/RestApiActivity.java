package com.example.myapplication;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;


public class RestApiActivity extends AppCompatActivity{
    private static final String TAG = "lifecycle";
    private TextView textView;
    private String url = "https://api.tronalddump.io/random/quote";
    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_api);
        Log.d(TAG, "RestApiActivity создано.");

        textView = (TextView) findViewById(R.id.textViewApi);

        try {
            run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void run() throws IOException{
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d(TAG, "failing");
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                assert response.body() != null;
                final String quote = response.body().string();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(quote);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    assert jsonObject != null;
                    result = jsonObject.getString("value");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, quote);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(result);
                    }
                });
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "RestApiActivity стал видимым.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "RestApiActivity получает фокус (состояние Resumed)");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "RestApiActivity приостановлено (состояние Paused)");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "RestApiActivity остановлено (состояние Stopped)");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "RestApiActivity перезапущено (состояние Restarted)");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "RestApiActivity уничтожено (состояние Destroyed)");
    }

}
